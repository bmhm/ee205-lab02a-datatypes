///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file long.h
/// @version 1.0
///
/// Print the characteristics of the "long" and "unsigned long" datatypes.
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   21 January 2021
///////////////////////////////////////////////////////////////////////////////

extern void doLong();            /// Print the characteristics of the "long" datatype
extern void flowLong();          /// Print the overflow/underflow characteristics of the "long" datatype

extern void doUnsignedLong();    /// Print the characteristics of the "unsigned long" datatype
extern void flowUnsignedLong();  /// Print the overflow/underflow characteristics of the "unsigned long" datatype

